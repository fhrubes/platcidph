<?php

class SubjectReportControl extends Nette\Application\UI\Control {
    
    private $accountRegistr;
    private $accountSap;
    private $subject;
    private $options;    
    private $paginator;
        
    private $registrPage = '';
    private $regPageTitle = '';
    private $sendTime = '&send_time=0';
    private $timeHash = '&send_time_hash=0';
    
    public function __construct(AccountRegistr $ar, AccountSAP $as, Subject $s, Array $options) {
        parent::__construct();
        $this->accountRegistr = $ar;
        $this->accountSap = $as;
        $this->subject = $s;
        $this->options = $options;        
        $this->paginator = new \Nette\Utils\Paginator();
        $this->paginator->setItemCount($this->subject->getSubjectsCount());
        $this->paginator->setItemsPerPage($this->options['itemsOnPage']);
    }
    
    public function handleSetPage($page) {
        $this->paginator->setPage($page);
        $this->invalidateControl('reportTable');
    }
    
    public function handleShowRegistrPageWindow($dic,$dic_sap,$nazev) {        
        $this->regPageTitle = $dic_sap.' - '.$nazev;        
        $this->registrPage = $this->getPage(array($dic));
        if($this->presenter->isAjax()) {            
            $this->invalidateControl('regPageWindow');
        }
    }   
    
    private function generateReportArray() {
        $reportArray = array();
        if($this->options['pouzeProblemove']) {
            $subjects = $this->subject->getSubjects();
        } else {
            $subjects = $this->subject->getSubjects()->limit($this->paginator->getLength(),$this->paginator->getOffset());
        }
        while ($subject = $subjects->fetch()) {            
            $this->presenter->compareAccounts($subject);
            if(!$this->options['pouzeProblemove'] || ($this->options['pouzeProblemove'] & ($subject->nespolehlivy === 'ANO' || (count($this->presenter->shodneUcty) == 0 & count($this->presenter->uctyRegistrShodne) == 0 & count($this->presenter->uctySapShodne) == 0) || count($this->presenter->uctySap) > 0))) {
                $reportArray[] = array(
                    'dic' => $subject->dic,
                    'dic_sap' => $subject->dic_sap,
                    'nazev_sap' => $subject->nazev_sap,
                    'cislo_sap' => $subject->cislo_sap,
                    'nespolehlivy' => $subject->nespolehlivy,
                    'nespolehlivy_od' => $subject->nespolehlivy_od,
                    'cislo_fu' => $subject->cislo_fu,
                    'shodne_ucty' => $this->presenter->shodneUcty,
                    'ucty_sap_shodne' => $this->presenter->uctySapShodne,
                    'ucty_registr_shodne' => $this->presenter->uctyRegistrShodne,
                    'ucty_sap' => $this->presenter->uctySap,
                    'ucty_registr' => $this->presenter->uctyRegistr
                );            
            }
            $this->presenter->shodneUcty = array();
            $this->presenter->uctySapShodne = array();
            $this->presenter->uctyRegistrShodne = array();
        }
        if($this->options['pouzeProblemove']) $this->paginator->setItemCount(count($reportArray));
        return $reportArray;
    }
    
    /**
     * Odešlě požadavek na zobrazení stránky s formulářem pro zadání DIČ
     * Z odpovědi vyparsuje cookies a další data potřebná pro odeslání validního
     * požadavku na zobrazení originální výpisu z registru plátců DPH
     * 
     * Při úspěchu vrací pole s potřebnými daty:
     * - javax    = kód vyparsovaný z HTML odpovědi
     * - cookies  = pole s vyparsovanými cookies
     * 
     * @return mixed
     */
    private function getRequestParams() {        
      $handle = curl_init();        

      curl_setopt($handle, CURLOPT_URL,'http://adisreg.mfcr.cz/adistc/DphReg?ZPRAC=FDPHI1');
      curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($handle, CURLOPT_HEADER, true);
      curl_setopt($handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
      curl_setopt($handle, CURLOPT_PROXY, '153.80.90.225:8080');
      curl_setopt($handle, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
      curl_setopt($handle, CURLOPT_PROXYUSERPWD, 'cz\internet:lt56ak');

      $response = trim(curl_exec($handle));
      curl_close($handle);

      $cookies = array();

      $header_end = strpos($response, '<html>');
      if ($header_end !== false && $header_end > 0)
      {
        $header_str = trim(substr($response, 0, $header_end+1));
        $header = explode("\n", $header_str);

        $cookies = array();

        for ($i=count($header)-1; $i>=0; $i--)
        {
          $pos = strpos(strtolower($header[$i]), 'set-cookie:');
          if ($pos === false)
            continue;

          $c = $header[$i];

          $cookie_parse = explode(':', $c);
          $cookie_parse = explode(';', $cookie_parse[1]);

          $cookies[] = trim($cookie_parse[0]);
        }
      }

      if (!count($cookies))
        return false;

      $javax = false;

      $key = 'javax.faces.ViewState';

      $javax_index = strpos($response, $key);
      if ($javax_index !== false) {
        $javax_parse = substr($response, $javax_index+strlen($key));

        $javax_index_begin = strpos(strtolower($javax_parse), 'value=');
        if (!$javax_index_begin)
          return false;

       $javax_index_end = strpos($javax_parse, '"', $javax_index_begin+strlen('value="'));

       $javax = substr($javax_parse, $javax_index_begin+strlen('value='), ($javax_index_end - $javax_index_begin) - 5);
       $javax = trim(str_replace(array('"','\''), '', $javax));
      }

      if (!$javax)
        return false;
      
      return array(
        'javax'   => $javax,
        'cookies' => $cookies,
      );     
    }
    
    private function getPage($dicArray = array()) {
      
      $params = $this->getRequestParams();
//      $params = false;
      if (!$params)
        return "Došlo k chybě při načítaní dat (neplatné parametry)";

      sleep(2);
      
      $dicArr = '';
      
      $i = 0;
      foreach($dicArray as $key => $dic) {
          $dicArr['form:dt:'.$i++.':inputDic'] = $dic;
      }
      
      $handle = curl_init();

      $post = array(
        'form:_idJsp31'         => '0',
        'form:_idJsp38'         => '2',
        'form:_idcl'            => '',
        'form:_link_hidden_'    => '',
        'form:hledej'           => 'Hledej',
        'form_SUBMIT'           => '1',
        'javax.faces.ViewState' => $params['javax'],
      );
      
      $post = array_merge($post, $dicArr);

      $cookies = implode("; ",$params['cookies']);

      curl_setopt($handle, CURLOPT_URL,'http://adisreg.mfcr.cz/adistc/adis/irs/irep_dph/dphInputForm.faces');
      curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'post');
      curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($handle, CURLOPT_COOKIE, $cookies);
      curl_setopt($handle, CURLOPT_POST, true);
      curl_setopt($handle, CURLOPT_POSTFIELDS, $post);
      curl_setopt($handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
      curl_setopt($handle, CURLOPT_PROXY, '153.80.90.225:8080');
      curl_setopt($handle, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
      curl_setopt($handle, CURLOPT_PROXYUSERPWD, 'cz\internet:lt56ak');

      $response = trim(curl_exec($handle));

      $base_position = strpos($response, '<body');
      if ($base_position !== false)
      {
        $base_end = strpos($response, '</body>', $base_position+strlen($base_position));
        
        $response = mb_substr($response, $base_position-5, ($base_end+strlen('</body>')+2)-$base_position, 'utf-8');
        
      }
      
      $response = str_replace('/adistc/DphReg?ZPRAC=FDPHI1', 'http://adisreg.mfcr.cz/adistc/DphReg?ZPRAC=FDPHI1', $response);
      
      curl_close($handle);

      return $response;
    }
    
    public function render() {
        $this->template->setFile(__DIR__ . '/SubjectReport.latte');
        $this->template->subjects = $this->generateReportArray();
        $this->template->opt = $this->options;        
        $this->template->pages = $this->paginator;
        $this->template->regPageTitle = $this->regPageTitle;
        $this->template->registrPage = $this->registrPage;        
        //$this->template->sap = $this->accountSap;
        $this->template->render();
    }
        
}