<?php

use \SoapClient;

class NTLMSoapClient extends SoapClient {
    
    private $proxy;
    
    public function __construct($url,$options,$proxy = TRUE) {
        $this->proxy = $proxy;
        if($this->proxy) {
            $this->proxy_login = $options['proxy_login'];
            $this->proxy_password = $options['proxy_password'];        
        }
        stream_wrapper_unregister('http');
        stream_wrapper_register('http', 'NTLMStream');
        parent::__construct($url,$options);
        
        stream_wrapper_restore('http');
        
    }
    
    function __doRequest($request, $location, $action, $version, $one_way = null) {
            $headers = array(
                    'Method: POST',
                    'Connection: Keep-Alive',
                    'User-Agent: PHP-SOAP-CURL',
                    'Content-Type: text/xml; charset=utf-8',
                    'SOAPAction: "'.$action.'"',
            );

            $this->__last_request_headers = $headers;
            $ch = curl_init($location);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, true );
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            if($this->proxy) {
                curl_setopt($ch, CURLOPT_PROXY, '153.80.90.225:8080');
                curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->proxy_login.':'.$this->proxy_password);
            }
            $response = curl_exec($ch);

            return $response;
    }

    function __getLastRequestHeaders() {
            return implode("\n", $this->__last_request_headers)."\n";
    }
    
    public function getFunctions()
    {
        return $this->__getFunctions();
    }
    
    public function sendRequest($dicList)
    {        
        return $this->getStatusNespolehlivyPlatce(array('dic' => $dicList));
    }
    
    public function responseToArray($response)
    {        
        $responseArray = array();  
        //$responseArray['responseDate'] = $response->status->odpovedGenerovana;        
        foreach($response->statusPlatceDPH as $statusPlatceDPH) {
            if(count($response->statusPlatceDPH) == 1) {
                $statusPlatceDPH = $response->statusPlatceDPH;
            }
            $dic = strval($statusPlatceDPH->dic);
            $responseArray[$dic]['nespolehlivy'] = $statusPlatceDPH->nespolehlivyPlatce;            
            //echo $dic;
            //print_r($resultData);
            if($statusPlatceDPH->nespolehlivyPlatce !== 'NENALEZEN') {
                $responseArray[$dic]['cisloFu'] = $statusPlatceDPH->cisloFu;
                
                if($statusPlatceDPH->nespolehlivyPlatce !== 'ANO') {
                    if(!isset($statusPlatceDPH->zverejneneUcty)) {                        
                        continue;
                    } else {
                        if(is_object($statusPlatceDPH->zverejneneUcty->ucet)) {
                            if(!isset($statusPlatceDPH->zverejneneUcty->ucet->nestandardniUcet)) {
                                $responseArray[$dic]['standardniUcetRegistr'][] = array(
                                    'datumZverejneni' => isset($statusPlatceDPH->zverejneneUcty->ucet->datumZverejneni) ? $statusPlatceDPH->zverejneneUcty->ucet->datumZverejneni : null,
                                    'predcisli' => isset($statusPlatceDPH->zverejneneUcty->ucet->standardniUcet->predcisli) ? $statusPlatceDPH->zverejneneUcty->ucet->standardniUcet->predcisli : 0,
                                    'cisloUctu' => $statusPlatceDPH->zverejneneUcty->ucet->standardniUcet->cislo,
                                    'kodBanky' => $statusPlatceDPH->zverejneneUcty->ucet->standardniUcet->kodBanky
                                );                                
                            } else {
                                $responseArray[$dic]['nestandardniUcetRegistr'][] = array(
                                    'datumZverejneni' => $statusPlatceDPH->zverejneneUcty->ucet->datumZverejneni,                                    
                                    'cisloUctu' => str_replace(' ','',$statusPlatceDPH->zverejneneUcty->ucet->nestandardniUcet->cislo)
                                );
                            }
                        } else if(is_array($statusPlatceDPH->zverejneneUcty->ucet)) {
                            foreach($statusPlatceDPH->zverejneneUcty->ucet as $ucet) {
                                if(!isset($ucet->nestandardniUcet)) {
                                    $responseArray[$dic]['standardniUcetRegistr'][] = array(
                                        'datumZverejneni' => isset($ucet->datumZverejneni) ? $ucet->datumZverejneni : null,
                                        'predcisli' => isset($ucet->standardniUcet->predcisli) ? $ucet->standardniUcet->predcisli : 0,
                                        'cisloUctu' => $ucet->standardniUcet->cislo,
                                        'kodBanky' => $ucet->standardniUcet->kodBanky
                                    );                                
                                } else {
                                    $responseArray[$dic]['nestandardniUcetRegistr'][] = array(
                                        'datumZverejneni' => $ucet->datumZverejneni,                                    
                                        'cisloUctu' => str_replace(' ','',$ucet->nestandardniUcet->cislo)
                                    );
                                }
                            }
                        }
                    }
                } else {
                    $responseArray[$dic]['datumZverejneniNespolehlivosti'] = $statusPlatceDPH->datumZverejneniNespolehlivosti;
                }
            }            
        }
        return $responseArray;
    }
    
}
       
class NTLMStream {
    private $proxy = TRUE;
    private $path;
    private $mode;
    private $options;
    private $opened_path;
    private $buffer;
    private $pos;
    
    /**
    * * Open the stream
    * *	
    * * @param unknown_type $path
    * * @param unknown_type $mode
    * * @param unknown_type $options
    * * @param unknown_type $opened_path
    * * @return unknown
    */	    
    public function stream_open($path, $mode, $options, $opened_path) {
        //echo "[NTLMStream::stream_open] $path , mode=$mode \n";
        $this->path = $path;
        $this->mode = $mode;
        $this->options = $options;
        $this->opened_path = $opened_path;
        $this->createBuffer($path);
        return true;
    }
    
    /** 	
     * * Close the stream   
    */	    
    public function stream_close() {
        //echo "[NTLMStream::stream_close] \n";
        curl_close($this->ch);
    }
    
    /**
     * * Read the stream
     * *
     * * @param int $count number of bytes to read
     * * @return content from pos to count
    */	    
    public function stream_read($count) {
        //echo "[NTLMStream::stream_read] $count \n";
        if(strlen($this->buffer) == 0) {
            return false;
        }
        $read = substr($this->buffer,$this->pos, $count);
        $this->pos += $count;
        return $read;
    }
    
    /**
    * * write the stream
    * *
    * * @param int $count number of bytes to read
    * * @return content from pos to count
    */	
    
    public function stream_write($data) {
        //echo "[NTLMStream::stream_write] \n";
        if(strlen($this->buffer) == 0) {
            return false;
        }
        return true;
    }
    
    /**
    *  * @return true if eof else false
    */	
    public function stream_eof() {
        //echo "[NTLMStream::stream_eof] ";
        if($this->pos > strlen($this->buffer)) {
            //echo "true \n";
            return true;
        }
        //echo "false \n";
        return false;
    }
    
    /**
    * * @return int the position of the current read pointer
    */    
    public function stream_tell() {
        //echo "[NTLMStream::stream_tell] \n";
        return $this->pos;
    }
    
    /**	 
    * * Flush stream data
    */	    
    public function stream_flush() {
        //echo "[NTLMStream::stream_flush] \n";
        $this->buffer = null;
        $this->pos = null;
    }
    
    /**
    * * Stat the file, return only the size of the buffer
    * * @return array stat information
    */    
    public function stream_stat() {
        //echo "[NTLMStream::stream_stat] \n";
        $this->createBuffer($this->path);
        $stat = array(
            'size' => strlen($this->buffer),
        );		
        return $stat;
        }
        
    /**
    * * Stat the url, return only the size of the buffer
    * * @return array stat information
    */	
        
    public function url_stat($path, $flags) {
        //echo "[NTLMStream::url_stat] \n";
        $this->createBuffer($path);
        $stat = array(
            'size' => strlen($this->buffer),
        );
        return $stat;
    }

    /**
    * * Create the buffer by requesting the url through cURL
    * * @param unknown_type $path
    */
    private function createBuffer($path) {
        if($this->buffer) {
            return;
        }
        //echo "[NTLMStream::createBuffer] create buffer from : $path\n";
        $this->ch = curl_init($path);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        if($this->proxy) {
            curl_setopt($this->ch, CURLOPT_PROXY, '153.80.90.225:8080');
            curl_setopt($this->ch, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
            curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, 'cz\internet:lt56ak');
        }
        $this->buffer = curl_exec($this->ch);
        //echo "[NTLMStream::createBuffer] buffer size : ".strlen($this->buffer)."bytes\n";
        $this->pos = 0;
    }
}