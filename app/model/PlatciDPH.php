<?php

abstract class PlatciDPH extends Nette\Object {
    
    protected $connection;
    protected $tableName;
    
    public function __construct(Nette\Database\Connection $db) {        
        $this->connection = $db;
    }
    
    public function getTable() {
        return $this->connection->table($this->tableName);
    }
    
    public function truncate() {
        return $this->connection->exec('DELETE FROM `'.$this->tableName.'`');
    }
    
}