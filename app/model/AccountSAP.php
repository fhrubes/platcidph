<?php

class AccountSAP extends PlatciDPH {
    
    protected $tableName = 'account_sap';
    
    public function getAccounts(Nette\Database\Table\ActiveRow $subject) {
        return $subject->related($this->tableName);
    }
    
}