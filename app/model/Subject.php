<?php

class Subject extends PlatciDPH {
    
    protected $tableName = 'subject';
    
    public function getRequestDate() {
        return $this->getTable()->min('datum_pozadavku');
    }
    
    public function getResponseDate() {
        return $this->getTable()->min('cas_odpovedi');
    }
    
    public function getSubjects() {
        return $this->getTable();
    }
    
    public function getSubjectsCount() {
        return $this->getTable()->count("*");
    }
    
    public function getSapAccountsOf(Nette\Database\Table\ActiveRow $subject) {
        return $subject->related('account_sap');
    }
    
    public function getRegistrAccountsOf(Nette\Database\Table\ActiveRow $subject) {
        return $subject->related('account_registr');
    }
    
}