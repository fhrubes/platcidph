<?php

class Csv
{
    private $path = '/csv/FI_KONTROLA_BU.CSV';
    private $archiv_path = '/csv_archiv/';
    private $delimiter = ';';
    private $filePointer;
    
    public function __construct() {
        if($this->sourceFileExists()) {
            $this->filePointer = fopen(WWW_DIR . $this->path, 'r');
            //$this->copyToArchive();
            $this->rewind();
        } else {
            throw new Nette\FileNotFoundException;
        }        
    }
    
    private function rewind()
    {
        rewind($this->filePointer);
    }
    
    private function sourceFileExists() {
        return file_exists(WWW_DIR . $this->path);
    }
    
    public function getFileCreationTime()
    {
        return filemtime(WWW_DIR . $this->path);
    }
    
    private function getNewFileName() {
        $newNamePrefix = 'CSV_';
        $newNameSuffix = date('Y-m-d_H-i-s', $this->getFileCreationTime(WWW_DIR . $this->path));
        //$newNameSuffix->format('yyyy-mm-dd_H-i-s');
        return $newNamePrefix . strval($newNameSuffix) . '.csv';
    }
    
    public function copyToArchive() {
        return copy(WWW_DIR . $this->path, WWW_DIR . $this->archiv_path . $this->getNewFileName());
    }
    
    public function isFileNewer() {
        $file = $this->getNewFileName();
        if(file_exists(WWW_DIR . $this->archiv_path . $file)) {
            return false;
        }
        return true;
    }

    public function dicList() 
    {        
        $this->rewind();
        $contain = array();
        $dicList = array();
        while($row = fgetcsv($this->filePointer, 1024, $this->delimiter)) {
            $dic = strval(doubleval(strtoupper(substr($row[0],0,-(strlen($row[0])-2))) == 'CZ' ? substr($row[0],2) : $row[0]));
            if(!isset($contain[$dic])) array_push($dicList, $dic);
            $contain[$dic] = 1;
        }        
        return $dicList;
    }
    
    public function csvToArray()
    {        
        $this->rewind();
        
        $dataArray = array();
        
        while($row = fgetcsv($this->filePointer, 1024, $this->delimiter)) {
            $dic = strval(doubleval(strtoupper(substr($row[0],0,-(strlen($row[0])-2))) == 'CZ' ? substr($row[0],2) : $row[0]));
            if(trim($row[4]) != '' & strlen($row[3]) < 20 & strlen($row[3]) > 2) {
                $kodBanky = strlen($row[4]) < 4 ? '0'.$row[4] : $row[4];
                if(strrpos($row[3],'-') > 0) {                    
                    $ucetArray = explode('-', $row[3]);
                    $predcisli = intval($ucetArray[0]);
                    $cisloUctu = doubleval($ucetArray[1]);                    
                } else {
                    $predcisli = 0;
                    $cisloUctu = doubleval($row[3]);                    
                }
                $dataArray[$dic]['standardniUcetSap'][] = array(
                    'predcisli' => strval($predcisli),
                    'cisloUctu' => strval($cisloUctu),
                    'kodBanky' => strval($kodBanky)
                );
            } else {
                $dataArray[$dic]['nestandardniUcetSap'][] = array(                    
                    'cisloUctu' => $row[3]                   
                );
            }
            $dataArray[$dic]['dic_sap'] = $row[0];
            $dataArray[$dic]['interniCisloSap'] = doubleval($row[1]);
            $dataArray[$dic]['nazevSubjektuSap'] = strval($row[2]);
        }    
        return $dataArray;
    }
}