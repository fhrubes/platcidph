<?php

define('OPTIONS_DIR', WWW_DIR . '/xml/');

class Options extends Nette\Object
{
    private $options_file = 'options.xml';
    private $xml;
    
    public $showSubjectNumber;
    public $showSubjectName;
    public $showAllAccounts;
    public $showAccountDate;
    public $showSubjectDate;
    public $showFuNumber;
    public $showOnlyProblems;
    public $excelOnlyProblems;
    public $defaultDicCount;
    public $itemsOnPage;
    public $acceptedAge;
    
    public function __construct()
    {
        $this->xml = simplexml_load_file(OPTIONS_DIR . $this->options_file);
        $this->showSubjectNumber = $this->getCisloSubjektu();
        $this->showSubjectName = $this->getNazevSubjektu();
        $this->showAllAccounts = $this->getUctyRegistru();
        $this->showAccountDate = $this->getDatumZverejneniUctu();
        $this->showSubjectDate = $this->getDatumZverejneniNespolehlivosti();
        $this->showFuNumber = $this->getCisloFu();
        $this->showOnlyProblems = $this->getPouzeProblemove();
        $this->excelOnlyProblems = $this->getExcelPouzeProblemove();
        $this->defaultDicCount = $this->getDefaultDicCount();
        $this->itemsOnPage = $this->getItemsOnPage();
        $this->acceptedAge = $this->getAcceptedAge();
    }    
    
    public function getCisloSubjektu() { return intval($this->xml->option->cisloSubjektu); }    
    public function getNazevSubjektu() { return intval($this->xml->option->nazevSubjektu); }    
    public function getUctyRegistru() { return intval($this->xml->option->uctyRegistru); }    
    public function getDatumZverejneniUctu() { return intval($this->xml->option->datumZverejneniUctu); }    
    public function getDatumZverejneniNespolehlivosti() { return intval($this->xml->option->datumZverejneniNespolehlivosti); }
    public function getCisloFu() { return intval($this->xml->option->cisloFu); }
    public function getPouzeProblemove() { return intval($this->xml->option->pouzeProblemove); }
    public function getExcelPouzeProblemove() { return intval($this->xml->option->excelPouzeProblemove); }
    public function getDefaultDicCount() { return intval($this->xml->option->defaultDicCount); }
    public function getItemsOnPage() { return intval($this->xml->option->itemsOnPage); }    
    public function getAcceptedAge() { return intval($this->xml->option->acceptedAge); }    
    
    public function setCisloSubjektu($value) { $this->xml->option->cisloSubjektu = $value; }
    public function setNazevSubjektu($value) { $this->xml->option->nazevSubjektu = $value; }
    public function setUctyRegistru($value) { $this->xml->option->uctyRegistru = $value; }
    public function setDatumZverejneniUctu($value) { $this->xml->option->datumZverejneniUctu = $value; }
    public function setDatumZverejneniNespolehlivosti($value) { $this->xml->option->datumZverejneniNespolehlivosti = $value; }
    public function setCisloFu($value) { $this->xml->option->cisloFu = $value; }
    public function setPouzeProblemove($value) { $this->xml->option->pouzeProblemove = $value; }
    public function setExcelPouzeProblemove($value) { $this->xml->option->excelPouzeProblemove = $value; }
    public function setDefaultDicCount($value) { $this->xml->option->defaultDicCount = $value; }
    public function setItemsOnPage($value) { $this->xml->option->itemsOnPage = $value; }
    public function setAcceptedAge($value) { $this->xml->option->acceptedAge = $value; }
    
    public function saveXml() {        
        $this->xml->asXML(OPTIONS_DIR . $this->options_file);
    }
}