<?php

use Nette\Application\Responses\FileResponse;

class HomepagePresenter extends BasePresenter
{          
    private $csv;
    private $subject;
    private $accountSAP;
    private $accountRegistr;
    private $ready;
    
    public $shodneUcty;
    public $uctySapShodne = array();
    public $uctyRegistrShodne = array();
    public $uctySap;
    public $uctyRegistr;
        
    private $acceptedAge;    // hours

    protected function startup() {
        parent::startup();        
        $this->subject = $this->context->subject;
        $this->accountSAP = $this->context->accountSap;
        $this->accountRegistr = $this->context->accountRegistr;
        $this->acceptedAge = $this->options['acceptedAge'];
        try {
            $this->csv = new Csv();
        } catch (Nette\FileNotFoundException $e) {
            $this->flashMessage('Soubor se zdrojovými daty ze SAPu nebyl nalezen. Error Message: ' . $e->getMessage(), 'error');
        }         
    }
    
    public function actionDefault($ready = false) {        
        $this->ready = $ready;
    }
    
    private function checkAge() {
        if($this->subject->getResponseDate() === NULL) {
            return false;
        }
        $currentTime = time() - $this->options['acceptedAge'] * 3600;
        $fileTime = strtotime($this->csv->getFileCreationTime());
        $dataTime = strtotime($this->subject->getResponseDate());
        if($dataTime < $currentTime || $dataTime < $fileTime) {            
            return false;
        }
        return true;
    }
    
    private function checkDataAge() {
        if($this->csv->isFileNewer()) {
            $this->csv->copyToArchive();
            $this->updateDataInDB();
        } else if(!$this->checkAge()) {
            $this->updateDataInDB();
        }        
    }
    
    private function updateDataInDB() {
        $this->accountSAP->truncate();
        $this->accountRegistr->truncate();
        $this->subject->truncate();
        $this->csvToDB();
        $this->soapToDB();
    }
    
    public function handleGetReady() {
        if(!$this->ready) {
            $this->checkDataAge();
            $this->ready = true;  
            $this->invalidateControl();
        }
        
    }
    
    public function compareAccounts($subject) {
        $sapAccount = $this->subject->getSapAccountsOf($subject);
        $registrAccount = $this->subject->getRegistrAccountsOf($subject);
        
        foreach($sapAccount as $ks => $sa) {
          $is_iban_sap = $this->is_iban($sa);
          
          foreach($registrAccount as $kr => $ra) {
            $is_iban_reg = $this->is_iban($ra);
            
            if ($is_iban_sap) {
              if ($is_iban_reg) {
                if (trim(strtoupper($sa['cisloUctu'])) != trim(strtoupper($ra['cisloUctu'])))
                  continue;
                
                $this->shodneUcty[] = $ra;
                unset($sapAccount[$ks]);
                unset($registrAccount[$kr]);
                
                continue;
              } else {
                $cu_reg = false;
                
                if (strpos($ra['cisloUctu'], '/') !== false)
                  $cu_reg = $this->parse_cu($ra['cisloUctu']);
                
                $cu_sap = $this->parse_iban($sa['cisloUctu']);
                if (!$cu_sap)
                  continue;
                
                $cu_reg = $cu_reg ? $cu_reg : $this->format_cu($ra);
                if ($cu_reg['predcisli'] != $cu_sap['predcisli'] || $cu_reg['cisloUctu'] != $cu_sap['cisloUctu'] || $cu_reg['kodBanky'] != $cu_sap['kodBanky'])
                  continue;
                
                $this->uctySapShodne[] = $sa;
                $this->uctyRegistrShodne[] = $ra;
                unset($sapAccount[$ks]);
                unset($registrAccount[$kr]);
              }
            } else {
              if ($is_iban_reg) {
                $cu_reg = $this->parse_iban($ra['cisloUctu']);
                if (!$cu_reg)
                  continue;                
                
                $cu_sap = $this->format_cu($sa);
                if ($cu_reg['predcisli'] != $cu_sap['predcisli'] || $cu_reg['cisloUctu'] != $cu_sap['cisloUctu'] || $cu_reg['kodBanky'] != $cu_sap['kodBanky'])
                  continue;
                
                
                if (trim(strtoupper($sa['cisloUctu'])) != trim(strtoupper($ra['cisloUctu'])))
                  continue;
                
                $this->uctySapShodne[] = $sa;
                $this->uctyRegistrShodne[] = $ra;
                unset($sapAccount[$ks]);
                unset($registrAccount[$kr]);                
                continue;
              } else {
                $cu_sap = $this->format_cu($sa);
                $cu_reg = $this->format_cu($ra);
                
                if ($cu_reg['predcisli'] != $cu_sap['predcisli'] || $cu_reg['cisloUctu'] != $cu_sap['cisloUctu'] || $cu_reg['kodBanky'] != $cu_sap['kodBanky'])
                  continue;
                
                $this->shodneUcty[] = $ra;
                unset($sapAccount[$ks]);
                unset($registrAccount[$kr]);
              }
            }
          }            
        }
                
        $this->uctySap = $sapAccount;
        $this->uctyRegistr = $registrAccount;        
    }
    
    private function format_cu($cu) {
      $cu_formated = array('predcisli' => false, 'cisloUctu' => false, 'kodBanky' => false);
      
      if ($cu['predcisli']) {
        $cu['predcisli'] = ltrim(trim($cu['predcisli']),'0');
      }
      
      if (!intval($cu['predcisli']))
        $cu_formated['predcisli'] = false;
      
      if ($cu['cisloUctu']) {
        $cu_formated['cisloUctu'] = ltrim(trim($cu['cisloUctu']),'0');
      }
      
      $cu_formated['kodBanky'] = trim($cu['kodBanky']);
      
      return $cu_formated;
    }
    
    private function is_iban($cu)
    {
      if (strpos($cu['cisloUctu'], '/') !== false)
        return false;
      
      if (strlen($cu['cisloUctu']) > 10)
        return true;
      
      return false;
    }
    
    private function is_iban_cz($iban) {
      return strtoupper(substr($iban, 0, 2)) == 'CZ';
    }
    
    /**
     * Z IBAN čísla vyparsuje tuzemský formát čísla účtu
     * 
     * @param string $iban IBAN
     * @return array
     */
    private function parse_iban($iban) {
      $iban = str_replace(' ', '', $iban);
      $iban = strtoupper(trim($iban));
      
      if (strlen($iban) != 24)
        return false;
      
      $country = substr($iban, 0, 2);
      $checksum = substr($iban, 2, 2);
      $kod = substr($iban, 4, 4);
      $predcisli = substr($iban, 8, 6);
      $cislo_uctu = substr($iban, 14);
      
      $parsed = array(
        'kodBanky'      => $kod,
        'predcisli'     => ltrim($predcisli, 0),
        'cisloUctu'     => ltrim($cislo_uctu, 0),
      );
      
      return $parsed;
    }
    
    private function get_iban($cisloUctu) {
      if ($cisloUctu && !is_array($cisloUctu))
        $cisloUctu = $this->parse_cu($cisloUctu);
      
      $iban = 'CZ00';
      
      $kod = '0000';
      if ($cisloUctu['kodBanky'])
        $kod = trim($cisloUctu['kodBanky']);
      
      $predcisli = '000000';
      if ($cisloUctu['predicisli'])
        $predcisli = str_pad(trim($cisloUctu['predcisli']), 6, '0', STR_PAD_LEFT);
            
      $cisloUctu = str_pad(trim($cisloUctu['cisloUctu']), 10, '0', STR_PAD_LEFT);
      
      $iban .= $kod . $predcisli . $cisloUctu;
      
      $iban_control = substr($iban, 4) . '1235' . substr($iban, 2, 4);
      
      $first = intval(substr($iban_control, 0, 9));
      $first = $first % 97;
      
      $second = intval(strval($first) . substr($iban_control, 9, 7));
      $second = $second % 97;
      
      $third = intval(strval($second) . substr($iban_control, 16, 7));
      $third = $third % 97;
      
      $last = intval(strval($third) . substr($iban_control, 23));
      $last = $last % 97;
      
      $control = strval(85 - $last);
      
      $iban = 'CZ'.str_pad($control, 2, '0', STR_PAD_LEFT).$kod.$predcisli.$cisloUctu;
      
      return $iban;
    }
    
    private function parse_cu($cu) {
      $kod_parsed = explode('/', $cu);
      if (count($kod_parsed) !== 2)
        return false;
      
      $kod = $kod_parsed[1];
      
      $predcisli = false;
      $cu = false;
      
      $cu_parsed = explode('-', $kod_parsed[0]);
      if (count($cu_parsed) === 1) {
        $cu = $cu_parsed[0];
      } elseif (count($cu_parsed) === 2) {
        if (trim($cu_parsed[0]) !== "")
          $predcisli = ltrim (trim ($cu_parsed[0]), '0');
        
        $cu = ltrim(trim($cu_parsed[1]),0);
      } else {
        return false;
      }
      
      return array(
        'predcisli' => $predcisli,
        'kodBanky'       => $kod,
        'cisloUctu'        => $cu,
      );
    }
    
    public function handleUpdate() {
        $this->updateDataInDB();
        $this->ready = true;
        $this->invalidateControl();
    }
    
    public function handleShowOnlyProblems($onlyProblems) {
        $this->options_xml->setPouzeProblemove(!$onlyProblems);
        $this->options_xml->saveXML();
        $this->options['pouzeProblemove'] = !$onlyProblems;
        if($this->presenter->isAjax()) {
            $this->ready = true;
            $this->invalidateControl();
        }
    }
    
    public function handleExportToExcel() {
        $this->checkDataAge();
        $excel = new PHPExcel();
        $excel->getProperties()->setCreator('DPH - NETTE Powered PHP Application');
        $excel->getProperties()->setCreated(date('d.m.Y H:i:s'));
        $excel->getProperties()->setCompany('VISCOFAN CZ, s.r.o.');        
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('DPH - '.date('d.m.Y'));        
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);        
        $excel->getActiveSheet()->getStyle('A4:F4')->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle('A4:F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $excel->getActiveSheet()->setCellValue('C1', 'Datum požadavku');
        $excel->getActiveSheet()->setCellValue('C2', 'Datum odpovědi');
        $excel->getActiveSheet()->setCellValue('D1', date_format($this->subject->getRequestDate(), 'd.m.Y - H:i:s'));
        $excel->getActiveSheet()->setCellValue('D2', date_format($this->subject->getResponseDate(), 'd.m.Y - H:i:s'));

        $excel->getActiveSheet()->setCellValue('A4', 'DIČ');
        $excel->getActiveSheet()->setCellValue('B4', 'Interní číslo SAP');
        $excel->getActiveSheet()->setCellValue('C4', 'Název subjektu SAP');
        $excel->getActiveSheet()->setCellValue('D4', 'Bankovní účty SAP');
        $excel->getActiveSheet()->setCellValue('E4', 'Bankovní účty Registr');
        $excel->getActiveSheet()->setCellValue('F4', 'Nespolehlivý plátce');
        
        $subjects = $this->subject->getSubjects();
        $iterator = 5;
        foreach($subjects as $subject) {            
            $this->compareAccounts($subject);
            if(!$this->options['excelPouzeProblemove'] || ($this->options['excelPouzeProblemove'] & ($subject->nespolehlivy === 'ANO' || (count($this->shodneUcty) == 0 & count($this->uctyRegistrShodne) == 0 & count($this->uctySapShodne) == 0) || count($this->uctySap) > 0))) {
                $counter = 0;
                if($this->shodneUcty !== NULL) {
                    $counter += count($this->shodneUcty);
                    foreach($this->shodneUcty as $ucet) {
                        $excel->getActiveSheet()->setCellValue('A' . $iterator, $subject->dic_sap);
                        $excel->getActiveSheet()->setCellValue('B' . $iterator, $subject->cislo_sap);
                        $excel->getActiveSheet()->setCellValue('C' . $iterator, $subject->nazev_sap);
                        $excel->getActiveSheet()->setCellValue('D' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('E' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('F' . $iterator, $subject->nespolehlivy);
                        $iterator++;
                    }
                }
                if($this->uctySapShodne !== NULL) {
                    $counter += count($this->uctySapShodne);
                    foreach($this->uctySapShodne as $ucet) {
                        $excel->getActiveSheet()->setCellValue('A' . $iterator, $subject->dic_sap);
                        $excel->getActiveSheet()->setCellValue('B' . $iterator, $subject->cislo_sap);
                        $excel->getActiveSheet()->setCellValue('C' . $iterator, $subject->nazev_sap);
                        $excel->getActiveSheet()->setCellValue('D' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        //$excel->getActiveSheet()->setCellValue('E' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('F' . $iterator, $subject->nespolehlivy);
                        $iterator++;
                    }
                }
                if($this->uctySap !== NULL) {
                    $counter += count($this->uctySap);
                    foreach($this->uctySap as $ucet) {
                        $excel->getActiveSheet()->setCellValue('A' . $iterator, $subject->dic_sap);
                        $excel->getActiveSheet()->setCellValue('B' . $iterator, $subject->cislo_sap);
                        $excel->getActiveSheet()->setCellValue('C' . $iterator, $subject->nazev_sap);
                        $excel->getActiveSheet()->setCellValue('D' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        //$excel->getActiveSheet()->setCellValue('E' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('F' . $iterator, $subject->nespolehlivy);
                        $iterator++;
                    }
                }
                if($this->uctyRegistrShodne !== NULL) {
                    $counter += count($this->uctyRegistrShodne);
                    foreach($this->uctyRegistrShodne as $ucet) {
                        $excel->getActiveSheet()->setCellValue('A' . $iterator, $subject->dic_sap);
                        $excel->getActiveSheet()->setCellValue('B' . $iterator, $subject->cislo_sap);
                        $excel->getActiveSheet()->setCellValue('C' . $iterator, $subject->nazev_sap);
                        //$excel->getActiveSheet()->setCellValue('D' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('E' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('F' . $iterator, $subject->nespolehlivy);
                        $iterator++;
                    }
                }
                if($this->uctyRegistr !== NULL) {
                    $counter += count($this->uctyRegistr);
                    foreach($this->uctyRegistr as $ucet) {
                        $excel->getActiveSheet()->setCellValue('A' . $iterator, $subject->dic_sap);
                        $excel->getActiveSheet()->setCellValue('B' . $iterator, $subject->cislo_sap);
                        $excel->getActiveSheet()->setCellValue('C' . $iterator, $subject->nazev_sap);
                        //$excel->getActiveSheet()->setCellValue('D' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('E' . $iterator, \Nette\Templating\Helpers::formatAccount($ucet));
                        $excel->getActiveSheet()->setCellValue('F' . $iterator, $subject->nespolehlivy);
                        $iterator++;
                    }
                }
                if($counter > 1) {
                    $first = $iterator - $counter;
                    $last = $iterator - 1;
                    $excel->getActiveSheet()->mergeCells('A'.  strval($first).':A'.  strval($last));
                    $excel->getActiveSheet()->mergeCells('B'.  strval($first).':B'.  strval($last));
                    $excel->getActiveSheet()->mergeCells('C'.  strval($first).':C'.  strval($last));
                    $excel->getActiveSheet()->mergeCells('F'.  strval($first).':F'.  strval($last));
                    $excel->getActiveSheet()->getStyle('A' . strval($first))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $excel->getActiveSheet()->getStyle('B' . strval($first))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $excel->getActiveSheet()->getStyle('C' . strval($first))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $excel->getActiveSheet()->getStyle('F' . strval($first))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                }
            }
            $this->shodneUcty = array();
            $this->uctySapShodne = array();
            $this->uctyRegistrShodne = array();
        }
        $phpExcelWriter = new PHPExcel_Writer_Excel2007($excel);
        $file = WWW_DIR . '/export/dph_'.date('dmY-His').'.xlsx';
        $phpExcelWriter->save($file);
        $this->sendResponse(new FileResponse($file));
    }
       
    private function csvToDB() {
        $csv_data = $this->csv->csvToArray();
        $file_creation = $this->csv->getFileCreationTime();
        foreach($csv_data as $dic => $data) {
            $subject = $this->subject->getTable()->insert(array(
                'dic' => $dic,
                'dic_sap' => $data['dic_sap'],
                'nazev_sap' => $data['nazevSubjektuSap'],
                'cislo_sap' => $data['interniCisloSap'],
                'datum_pozadavku' => date('Y-m-d H:i:s', $file_creation)
            ));
            $this->csvAccountToDB($subject, isset($data['standardniUcetSap']) ? $data['standardniUcetSap'] : array(), isset($data['nestandardniUcetSap']) ? $data['nestandardniUcetSap'] : array());
        }
    }
    
    private function csvAccountToDB(Nette\Database\Table\ActiveRow $subject, $standardniUcet, $nestandardniUcet) {
        if(!empty($standardniUcet)) {
            foreach($standardniUcet as $ucet) {
                $this->accountSAP->getTable()->insert(array(
                    'subject_id' => $subject->id,
                    'predcisli' => $ucet['predcisli'],
                    'cisloUctu' => $ucet['cisloUctu'],
                    'kodBanky' => $ucet['kodBanky']
                ));
            }
        }
        if(!empty($nestandardniUcet)) {
            foreach($nestandardniUcet as $ucet) {
                $this->accountSAP->getTable()->insert(array(
                    'subject_id' => $subject->id,
                    'cisloUctu' => $ucet['cisloUctu']
                ));
            }
        }
    }
    
    private function setPagination($itemsOnPage, $itemsCount) {
        $pagination = array();
        $startItem = 0;
        if($itemsOnPage < $itemsCount) $endItem = $itemsOnPage - 1;
        else $endItem = $itemsCount - 1;
        $pagination[] = array(
            'startItem' => $startItem,
            'endItem' => $endItem
        );
        while($endItem < ($itemsCount - 1)) {
            $startItem = $endItem + 1;
            if(($endItem + $itemsOnPage) < $itemsCount) $endItem += $itemsOnPage;
            else $endItem = $itemsCount - 1;
            $pagination[] = array(
                'startItem' => $startItem,
                'endItem' => $endItem
            );
        }
        return $pagination;
    }
    
    private function soapToDB() {
        $soap = new NTLMSoapClient($this->url,$this->proxy_setting);
        $dicList = $this->csv->dicList();
        $paginator = $this->setPagination(100, count($dicList));
                
        foreach($paginator as $page) {            
            $soap_response = $soap->sendRequest(array_slice($dicList, $page['startItem'], $page['endItem']+1));
            $soap_data = $soap->responseToArray($soap_response);
            foreach ($soap_data as $dic => $data) {
                $this->subject->getTable()->where(array('dic' => strval($dic)))
                        ->update(array(
                            'nespolehlivy' => isset($data['nespolehlivy']) ? $data['nespolehlivy'] : NULL,
                            'nespolehlivy_od' => isset($data['datumZverejneniNespolehlivosti']) ? $data['datumZverejneniNespolehlivosti'] : NULL,
                            'cislo_fu' => isset($data['cisloFu']) ? $data['cisloFu'] : NULL,
                            'cas_odpovedi' => date('Y-m-d H:i:s', time())
                        ));
                $this->soapAccountToDB($this->subject->getTable()->where(array('dic' => strval($dic)))->fetch(), isset($data['standardniUcetRegistr']) ? $data['standardniUcetRegistr'] : array(), isset($data['nestandardniUcetRegistr']) ? $data['nestandardniUcetRegistr'] : array());
            }
        }
        
    }
    
    private function soapAccountToDB(Nette\Database\Table\ActiveRow $subject, $standardniUcet, $nestandardniUcet) {
        if(!empty($standardniUcet)) {
            foreach($standardniUcet as $ucet) {
                $this->accountRegistr->getTable()->insert(array(
                    'subject_id' => $subject->id,
                    'predcisli' => $ucet['predcisli'],
                    'cisloUctu' => $ucet['cisloUctu'],
                    'kodBanky' => $ucet['kodBanky'],
                    'datum_zverejneni' => $ucet['datumZverejneni']
                ));
            }
        }
        if(!empty($nestandardniUcet)) {
            foreach($nestandardniUcet as $ucet) {
                $this->accountRegistr->getTable()->insert(array(
                    'subject_id' => $subject->id,
                    'cisloUctu' => $ucet['cisloUctu'],
                    'datum_zverejneni' => $ucet['datumZverejneni']
                ));
            }
        }
    }
    
    protected function createComponentSubjectReport() {
        return new SubjectReportControl($this->accountRegistr, $this->accountSAP, $this->subject, $this->options);
    }
     
    public function renderDefault() { 
        $this->template->requestTime = date_format($this->subject->getRequestDate(), 'd.m.Y - H:i:s');
        if($this->subject->getResponseDate() === NULL) {
            $this->template->responseTime = NULL;
        } else {
            $this->template->responseTime = date_format($this->subject->getResponseDate(), 'd.m.Y - H:i:s');
        }
        $this->template->options = $this->options;
        $this->template->ready = $this->ready;
    }
        
}
