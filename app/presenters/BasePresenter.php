<?php

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    public $options_xml;
    public $options = array();
    
    public $url = 'http://adisrws.mfcr.cz/adistc/axis2/services/rozhraniCRPDPH.rozhraniCRPDPHSOAP';
    public $proxy_setting = array(
        'proxy_host' => '153.80.90.225',
        'proxy_port' => 8080,
        'proxy_login' => 'cz\internet',
        'proxy_password' => 'lt56ak'
    );
    
    protected function startup()
    {
        parent::startup();
        $this->options_xml = new Options();
        $this->options = $this->getOptions();        
    }
    
    private function getOptions() {
        $options = array(
            'cisloSubjektu' => $this->options_xml->getCisloSubjektu(),
            'nazevSubjektu' => $this->options_xml->getNazevSubjektu(),
            'uctyRegistru' => $this->options_xml->getUctyRegistru(),
            'datumZverejneniUctu' => $this->options_xml->getDatumZverejneniUctu(),
            'datumZverejneniNespolehlivosti' => $this->options_xml->getDatumZverejneniNespolehlivosti(),
            'cisloFu' => $this->options_xml->getCisloFu(),
            'pouzeProblemove' => $this->options_xml->getPouzeProblemove(),
            'excelPouzeProblemove' => $this->options_xml->getExcelPouzeProblemove(),
            'defaultDicCount' => $this->options_xml->getDefaultDicCount(),
            'itemsOnPage' => $this->options_xml->getItemsOnPage(),
            'acceptedAge' => $this->options_xml->getAcceptedAge()
        );
        return $options;
    }    
    
}
