<?php
use Nette\Application\UI\Form;

class SimpleRequestPresenter extends BasePresenter
{
    private $soap;   
    
    private $dicList = array();
    
    private $registrPage = array();    
    private $sendTime = '&send_time=0';
    private $timeHash = '&send_time_hash=0';
    
    /** @persistent */
    public $dicCount = null;
    
    protected function startup()
    {
        parent::startup();        
        if($this->dicCount == null) $this->dicCount = $this->options['defaultDicCount'];
        $this->options['uctyRegistru'] = 1;
        $this->options['datumZverejneniUctu'] = 1;
        $this->options['pouzeProblemove'] = 0;
    }
    
    public function actionShowResponse($dicParametr, $dicString) {
        $this->registrPage = $this->getPage($dicParametr);
        $this->dicList = explode('-', $dicString);
    }
    
    public function handleChangeDicCount($count) {
        if($count > 0 & $count <= 100) {
            $this->dicCount = $count;
        }
        if($this->isAjax()) {
            $this->invalidateControl();
        } else {
            $this->redirect('this');
        }
    }
    
    /**
     * Odešlě požadavek na zobrazení stránky s formulářem pro zadání DIČ
     * Z odpovědi vyparsuje cookies a další data potřebná pro odeslání validního
     * požadavku na zobrazení originální výpisu z registru plátců DPH
     * 
     * Při úspěchu vrací pole s potřebnými daty:
     * - javax    = kód vyparsovaný z HTML odpovědi
     * - cookies  = pole s vyparsovanými cookies
     * 
     * @return mixed
     */
    private function getRequestParams() {        
      $handle = curl_init();        

      curl_setopt($handle, CURLOPT_URL,'http://adisreg.mfcr.cz/adistc/DphReg?ZPRAC=FDPHI1');
      curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($handle, CURLOPT_HEADER, true);
      curl_setopt($handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
      curl_setopt($handle, CURLOPT_PROXY, '153.80.90.225:8080');
      curl_setopt($handle, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
      curl_setopt($handle, CURLOPT_PROXYUSERPWD, 'cz\internet:lt56ak');
      
      $response = trim(curl_exec($handle));
      curl_close($handle);

      $cookies = array();

      $header_end = strpos($response, '<html>');
      if ($header_end !== false && $header_end > 0)
      {
        $header_str = trim(substr($response, 0, $header_end+1));
        $header = explode("\n", $header_str);

        $cookies = array();

        for ($i=count($header)-1; $i>=0; $i--)
        {
          $pos = strpos(strtolower($header[$i]), 'set-cookie:');
          if ($pos === false)
            continue;

          $c = $header[$i];

          $cookie_parse = explode(':', $c);
          $cookie_parse = explode(';', $cookie_parse[1]);

          $cookies[] = trim($cookie_parse[0]);
        }
      }

      if (!count($cookies))
        return false;

      $javax = false;

      $key = 'javax.faces.ViewState';

      $javax_index = strpos($response, $key);
      if ($javax_index !== false) {
        $javax_parse = substr($response, $javax_index+strlen($key));

        $javax_index_begin = strpos(strtolower($javax_parse), 'value=');
        if (!$javax_index_begin)
          return false;

       $javax_index_end = strpos($javax_parse, '"', $javax_index_begin+strlen('value="'));

       $javax = substr($javax_parse, $javax_index_begin+strlen('value='), ($javax_index_end - $javax_index_begin) - 5);
       $javax = trim(str_replace(array('"','\''), '', $javax));
      }

      if (!$javax)
        return false;
      
      return array(
        'javax'   => $javax,
        'cookies' => $cookies,
      );     
    }
    
    private function getPage($dicArray = array()) {
      
      $params = $this->getRequestParams();
//      $params = false;
      if (!$params)
        return "Došlo k chybě při načítaní dat (neplatné parametry)";

      sleep(2);
      
      $dicArray = unserialize($dicArray);
      
      $dicArr = array();
      
      $i = 0;
      foreach($dicArray as $key => $dic) {
          $dicArr['form:dt:'.$i++.':inputDic'] = $dic;
      }
      
      $handle = curl_init();

      $post = array(
        'form:_idJsp31'         => '0',
        'form:_idJsp38'         => '2',
        'form:_idcl'            => '',
        'form:_link_hidden_'    => '',
        'form:hledej'           => 'Hledej',
        'form_SUBMIT'           => '1',
        'javax.faces.ViewState' => $params['javax'],
      );
      
      $post = array_merge($post, $dicArr);

      $cookies = implode("; ",$params['cookies']);

      curl_setopt($handle, CURLOPT_URL,'http://adisreg.mfcr.cz/adistc/adis/irs/irep_dph/dphInputForm.faces');
      curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'post');
      curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($handle, CURLOPT_COOKIE, $cookies);
      curl_setopt($handle, CURLOPT_POST, true);
      curl_setopt($handle, CURLOPT_POSTFIELDS, $post);
      curl_setopt($handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);        
      curl_setopt($handle, CURLOPT_PROXY, '153.80.90.225:8080');
      curl_setopt($handle, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
      curl_setopt($handle, CURLOPT_PROXYUSERPWD, 'cz\internet:lt56ak');

      $response = trim(curl_exec($handle));
      curl_close($handle);
      
      $result = array();
            
      
      $base_position = strpos($response, '<body');

      if ($base_position !== false)
      {
        $base_end = strpos($response, '</body>', $base_position+strlen($base_position));

        $res = mb_substr($response, $base_position-5, ($base_end+strlen('</body>')+2)-$base_position, 'utf-8');
        $res = str_replace('/adistc/DphReg?ZPRAC=FDPHI1', 'http://adisreg.mfcr.cz/adistc/DphReg?ZPRAC=FDPHI1', $response);
        
        $result[] = $res;

        
      }
        
      return $result;
    }
    
    protected function createComponentSimpleRequestForm() {
        $form = new Form();
        for($i=0;$i<$this->dicCount;$i++) {
            $form->addText('dic_'.$i, 'CZ')
                    ->setRequired('Všechna pole musí být vyplněna!');
        }
        $form->addSubmit('sendRequest','Poslat požadavek');
        
        $form->onSuccess[] = callback($this, 'simpleRequestFormSubmitted');
        
        return $form;
    }
    
    public function simpleRequestFormSubmitted(Form $form) {
        $values = $form->getValues();        
        
        $dicArr = array();
        foreach($values as $key => $value) {
          $dic = str_replace('CZ', '', $value);
          $dicArr[] = $dic;
        }
                
        $this->redirect('SimpleRequest:showResponse', serialize($dicArr));
    }
           
    public function renderShowResponse() {
        $this->template->registrPage = $this->registrPage;
        $this->template->dicList = $this->dicList;
    }
    
    public function renderDefault() {
        $this->template->dicCount = $this->dicCount;
    }
}
