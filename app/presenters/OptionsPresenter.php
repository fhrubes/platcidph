<?php

use Nette\Application\UI\Form;

class OptionsPresenter extends BasePresenter
{
    private $xml;
    
    protected function startup()
    {
        parent::startup();
        $this->xml = $this->options_xml;
    }
    
    protected function createComponentOptionsForm()
    {
        $form = new Form();
        
        $form->addGroup('Nastavení sloupců');
        $form->addCheckbox('cisloSubjektu', 'Zobrazit interní číslo subjektu ze SAPu');
        $form->addCheckbox('nazevSubjektu', 'Zobrazit název subjektu ze SAPu');
        $form->addCheckbox('uctyRegistru', 'Zobrazit všechny účty z registru');
        $form->addCheckbox('datumZverejneniUctu', 'Zobrazit datum zveřejnění účtu');
        $form->addCheckbox('datumZverejneniNespolehlivosti', 'Zobrazit datum zveřejnění nespolehlivosti');
        $form->addCheckbox('cisloFu', 'Zobrazit číslo finančního úřadu');
        $form->addCheckbox('pouzeProblemove', 'Zobrazit pouze problémové subjekty');        
        $form->addGroup('Nastavení exportu do formátu Excel');
        $form->addCheckbox('excelPouzeProblemove', 'V dokumentu Excel zobrazit pouze problémové subjekty');        
        $form->addGroup('Jednotlivé dotazy');
        $form->addSelect('defaultDicCount', 'Výchozí počet zobrazených DIČ ve formuláři', array(
            '1' => 1,
            '2' => 2,
            '3' => 3,
            '4' => 4,
            '5' => 5,
            '6' => 6,
            '7' => 7,
            '8' => 8,
            '9' => 9,
            '10' => 10
        ));
        $form->addGroup('Nastavení počtu položek');
        $form->addSelect('itemsOnPage', 'Počet zobrazených položek na stránce', array(
            '10' => 10,
            '20' => 20,
            '30' => 30,
            '40' => 40,
            '50' => 50,
            '60' => 60,
            '70' => 70,
            '80' => 80,
            '90' => 90,
            '100' => 100
        ));
        $form->addGroup('Maximální stáří dat z registru (hodin)');
        $form->addSelect('acceptedAge', 'Hodin: ', array(
            0 => '0 hodin',
            1 => '1 hodina',
            2 => '2 hodiny',
            3 => '3 hodiny',
            4 => '4 hodiny',
            5 => '5 hodin',
            12 => '12 hodin',
            24 => '24 hodin',
            48 => '48 hodin'
        ));
        
        $form->setDefaults(array(
            'cisloSubjektu' => $this->xml->getCisloSubjektu(),
            'nazevSubjektu' => $this->xml->getNazevSubjektu(),
            'uctyRegistru' => $this->xml->getUctyRegistru(),
            'datumZverejneniUctu' => $this->xml->getDatumZverejneniUctu(),
            'datumZverejneniNespolehlivosti' => $this->xml->getDatumZverejneniNespolehlivosti(),
            'cisloFu' => $this->xml->getCisloFu(),
            'pouzeProblemove' => $this->xml->getPouzeProblemove(),
            'excelPouzeProblemove' => $this->xml->getExcelPouzeProblemove(),
            'defaultDicCount' => $this->xml->getDefaultDicCount(),
            'itemsOnPage' => $this->xml->getItemsOnPage(),
            'acceptedAge' => $this->xml->getAcceptedAge()
        ));
        
        $form->addGroup('Uložení nastavení');
        $form->addSubmit('saveOptions', 'Uložit nastavení');
        $form->onSuccess[] = callback($this, 'optionsFormSubmitted');
        
        return $form;
    }
    
    public function optionsFormSubmitted(Form $form) {
        $options = $form->getValues();
        $options['cisloSubjektu'] == 1 ? $this->xml->setCisloSubjektu($options['cisloSubjektu']) : $this->xml->setCisloSubjektu(0);
        $options['nazevSubjektu'] == 1 ? $this->xml->setNazevSubjektu($options['nazevSubjektu']) : $this->xml->setNazevSubjektu(0);
        $options['uctyRegistru'] == 1 ? $this->xml->setUctyRegistru($options['uctyRegistru']) : $this->xml->setUctyRegistru(0);
        $options['datumZverejneniUctu'] == 1 ? $this->xml->setDatumZverejneniUctu($options['datumZverejneniUctu']) : $this->xml->setDatumZverejneniUctu(0);
        $options['datumZverejneniNespolehlivosti'] == 1 ? $this->xml->setDatumZverejneniNespolehlivosti($options['datumZverejneniNespolehlivosti']) : $this->xml->setDatumZverejneniNespolehlivosti(0);
        $options['cisloFu'] == 1 ? $this->xml->setCisloFu($options['cisloFu']) : $this->xml->setCisloFu(0);
        $options['pouzeProblemove'] == 1 ? $this->xml->setPouzeProblemove($options['pouzeProblemove']) : $this->xml->setPouzeProblemove(0);
        $options['excelPouzeProblemove'] == 1 ? $this->xml->setExcelPouzeProblemove($options['excelPouzeProblemove']) : $this->xml->setExcelPouzeProblemove(0);
        $this->xml->setDefaultDicCount($options['defaultDicCount']);
        $this->xml->setItemsOnPage($options['itemsOnPage']);
        $this->xml->setAcceptedAge($options['acceptedAge']);
        
        $this->xml->saveXml();
        
        $this->flashMessage('Nastavení bylo uloženo');
        
        $this->redirect('this');
    }
    
    public function renderDefault() {
        
    }
}