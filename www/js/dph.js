function openRegistrPageWindow()
{
    clearElement();
    var about = document.getElementById("registrPageWindow");
    about.style.display = "block";
}
function closeRegistrPageWindow()
{
    var about = document.getElementById("registrPageWindow");
    about.style.display = "none";
    clearElement();
    
}

function clearElement() {
    var registrDiv = document.getElementById("registrPageDiv");
    registrDiv.innerHTML = '<div class="progress_layer" style="text-align:center;"><img src="../images/indicator.gif" style="margin-top: 100px;"></div>';
}

function showProgress() {
    var window = document.getElementById("resultTableDiv");
    window.innerHTML = '<div class="transparent_layer" style="top:72px;left:0;text-align:center;"><img src="../images/indicator.gif" style="margin-top: 200px;"></div>';
}
