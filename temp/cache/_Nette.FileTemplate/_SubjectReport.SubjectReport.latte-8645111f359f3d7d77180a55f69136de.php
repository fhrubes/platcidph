<?php //netteCache[01]000396a:2:{s:4:"time";s:21:"0.45294300 1476975668";s:9:"callbacks";a:2:{i:0;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:9:"checkFile";}i:1;s:74:"C:\web_projects\platcidph\app\components\SubjectReport\SubjectReport.latte";i:2;i:1476975658;}i:1;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:10:"checkConst";}i:1;s:25:"Nette\Framework::REVISION";i:2;s:30:"6a33aa6 released on 2012-10-01";}}}?><?php

// source file: C:\web_projects\platcidph\app\components\SubjectReport\SubjectReport.latte

?><?php
// prolog Nette\Latte\Macros\CoreMacros
list($_l, $_g) = Nette\Latte\Macros\CoreMacros::initRuntime($template, 'htukl3r01y')
;
// prolog Nette\Latte\Macros\UIMacros
//
// block _reportTable
//
if (!function_exists($_l->blocks['_reportTable'][] = '_lb7cf7de7cab__reportTable')) { function _lb7cf7de7cab__reportTable($_l, $_args) { extract($_args); $_control->validateControl('reportTable')
?><script>
    $(document).ready(function() {
        $('#reportTable').fixheadertable({            
            colratio : [30,100,<?php if ($opt['cisloSubjektu']): ?>75,<?php endif ;if ($opt['nazevSubjektu']): ?>
250,<?php endif ?>100,<?php if ($opt['datumZverejneniNespolehlivosti']): ?>100,<?php endif ;if ($opt['cisloFu']): ?>
50,<?php endif ?>200,200,<?php if ($opt['datumZverejneniUctu']): ?>100,<?php endif ?>110],
            //sortable : true,
            //sortedColId : 3,
            //sortType : ['integer','integer',<?php if ($opt['cisloSubjektu']): ?>
'integer',<?php endif ;if ($opt['nazevSubjektu']): ?>'string',<?php endif ?>'string',<?php if ($opt['datumZverejneniNespolehlivosti']): ?>
'date',<?php endif ;if ($opt['cisloFu']): ?>'string',<?php endif ?>'integer','integer',<?php if ($opt['datumZverejneniUctu']): ?>
'date',<?php endif ?>'string'],
            resizeCol: true,
            wrapper: false
        });
        $( ".jqbutton" ).button();
        $("#registrPageWindow").dialog({            
            minHeight: 400,
            maxHeight: 600,
            autoOpen: false,            
            width: 590,
            modal: false,            
            position: { my: "center top", at: "center top", of: window },
            open: function(event, ui) {
                
            },
            close: function(event, ui) {
                if ( event.originalEvent && $(event.originalEvent.target).closest(".ui-dialog-titlebar-close").length ) {                    
                    activeRow.children( "td" ).css({
                        backgroundColor: defaultBack,
                        color: "#222222",
                        fontWeight: "normal",
                        textDecoration: "none"
                    });
                }
            },
            show: {
                efect: "blind",
                duration: 500
            },
            hide: {
                efect: "blind",
                duration: 500
            }
        });
        $(".registrDialogOpen").live("click", function() {
            var $this = $( this );                
            activeRow = $this.closest("tr");
            defaultBack = activeRow.css("backgroundColor");
            activeRow.children( "td" ).css({                
                color: '#000000',
                backgroundColor: '#D0D0D0',
                textDecoration: "underline"
            });            
            $("#registrPageWindow").dialog("option","title","Výpis z registru");
            $("#registrPageWindow").dialog("open");
        });
    });
</script>

<div class="reportPagesContainer">
    <table style="border-collapse:collapse;width:100%;">
        <tr>
            <td style="text-align:left;width:50px;">
                <div style="height: 100%;text-align:left;float:left;">
<?php if ($pages->getPage() > 1): ?>
                    <a class="jqbutton ajax" style="line-height:20px;vertical-align:middle;" href="<?php echo htmlSpecialChars($_control->link("setPage!", array($pages->getPage()-1))) ?>
">Předchozí</a>
<?php endif ?>
                </div>
            </td>
            <td style="text-align:center;">
                <div style="height: 100%;font-weight: bold;">
                    <span style="line-height:20px;vertical-align:middle;">
<?php if (!$opt['pouzeProblemove']): ?>
                            <?php echo Nette\Templating\Helpers::escapeHtml($pages->getOffset()+1, ENT_NOQUOTES) ?>
 - <?php echo Nette\Templating\Helpers::escapeHtml($pages->getLength()+$pages->getOffset(), ENT_NOQUOTES) ?>
 / <?php echo Nette\Templating\Helpers::escapeHtml($pages->getItemCount(), ENT_NOQUOTES) ?>

<?php else: ?>
                            <?php echo Nette\Templating\Helpers::escapeHtml($pages->getOffset()+1, ENT_NOQUOTES) ?>
 - <?php echo Nette\Templating\Helpers::escapeHtml($pages->getItemCount(), ENT_NOQUOTES) ?>
 / <?php echo Nette\Templating\Helpers::escapeHtml($pages->getItemCount(), ENT_NOQUOTES) ?>

<?php endif ?>
                    </span>
                </div>
            </td>
            <td style="text-align:right;width:50px;">
                <div style="height: 100%;text-align:right;float:right;">
<?php if ($pages->getPage() < $pages->getPageCount() && !$opt['pouzeProblemove']): ?>
                        <a class="jqbutton ajax" style="line-height:20px;vertical-align:middle;" href="<?php echo htmlSpecialChars($_control->link("setPage!", array($pages->getPage()+1))) ?>
">Další</a>
<?php endif ?>
                </div>
            </td>
        </tr>
    </table>
</div>

<div class="reportTableContainer">
    <table id="reportTable" class="reportTable" cellpadding="0" cellspacing="0">
        <thead>
            <tr class="reportTableHeading">
                <th> </th>
                <th>DIČ</th>
<?php if ($opt['cisloSubjektu']): ?>
                    <th>Interní číslo<br />SAP</th>
<?php endif ;if ($opt['nazevSubjektu']): ?>
                    <th>Název subjektu<br />SAP</th>
<?php endif ?>
                <th>Nespolehlivý<br />plátce</th>
<?php if ($opt['datumZverejneniNespolehlivosti']): ?>
                    <th>Datum zveřejnění<br />nespolehlivosti</th>
<?php endif ;if ($opt['cisloFu']): ?>
                    <th>Číslo<br />FÚ</th>
<?php endif ?>
                <th>Bankovní účty<br />SAP</th>
                <th>Bankovní účty<br />Registr</th>
<?php if ($opt['datumZverejneniUctu']): ?>
                    <th>Datum zveřejnění<br />účtu</th>
<?php endif ?>
                <th>Registr</th>
            </tr>
        </thead>
        <tbody>
<?php $iterations = 0; foreach ($iterator = $_l->its[] = new Nette\Iterators\CachingIterator($subjects) as $subject): ?>
        <tr<?php if ($_l->tmp = array_filter(array($iterator->odd ? 'reportTableOddRow' : 'reportTableEvenRow'))) echo ' class="' . htmlSpecialChars(implode(" ", array_unique($_l->tmp))) . '"' ?>>
            <td style="text-align:center;"><?php echo Nette\Templating\Helpers::escapeHtml($iterator->getCounter()+$pages->getOffset(), ENT_NOQUOTES) ?></td>
            <td style=""><?php echo Nette\Templating\Helpers::escapeHtml($subject['dic_sap'], ENT_NOQUOTES) ?></td>
<?php if ($opt['cisloSubjektu']): ?>
                <td style=""><?php echo Nette\Templating\Helpers::escapeHtml($subject['cislo_sap'], ENT_NOQUOTES) ?></td>
<?php endif ;if ($opt['nazevSubjektu']): ?>
                <td style=""><?php echo Nette\Templating\Helpers::escapeHtml($subject['nazev_sap'], ENT_NOQUOTES) ?></td>
<?php endif ?>
            <td style="text-align:center;"><?php echo Nette\Templating\Helpers::escapeHtml($subject['nespolehlivy'], ENT_NOQUOTES) ?></td>
<?php if ($opt['datumZverejneniNespolehlivosti']): ?>
                <td style="text-align:center;"><?php echo Nette\Templating\Helpers::escapeHtml($subject['nespolehlivy_od'] === NULL ? '' : date_format($subject['nespolehlivy_od'],'d.m.Y'), ENT_NOQUOTES) ?></td>
<?php endif ;if ($opt['cisloFu']): ?>
                <td style="text-align:center;"><?php echo Nette\Templating\Helpers::escapeHtml($subject['cislo_fu'], ENT_NOQUOTES) ?></td>
<?php endif ?>
            <td style="">
<?php $iterations = 0; foreach ($subject['shodne_ucty'] as $ucet): ?>
                    <div class="shodnyUcet">
                        <?php echo Nette\Templating\Helpers::escapeHtml($template->formatAccount($ucet), ENT_NOQUOTES) ?>

                    </div>
<?php $iterations++; endforeach ;$iterations = 0; foreach ($subject['ucty_sap_shodne'] as $ucet): ?>
                    <div class="shodnyUcetSap">
                        <?php echo Nette\Templating\Helpers::escapeHtml($template->formatAccount($ucet), ENT_NOQUOTES) ?>

                    </div>
<?php $iterations++; endforeach ;$iterations = 0; foreach ($subject['ucty_sap'] as $ucet): ?>
                    <div class="ucetSap">
                        <?php echo Nette\Templating\Helpers::escapeHtml($template->formatAccount($ucet), ENT_NOQUOTES) ?>

                    </div>
<?php $iterations++; endforeach ?>
            </td>
            <td style="">
<?php $iterations = 0; foreach ($subject['shodne_ucty'] as $ucet): ?>
                    <div class="shodnyUcet">
                        <?php echo Nette\Templating\Helpers::escapeHtml($template->formatAccount($ucet), ENT_NOQUOTES) ?>

                    </div>
<?php $iterations++; endforeach ;$iterations = 0; foreach ($subject['ucty_registr_shodne'] as $ucet): ?>
                    <div class="shodnyUcetRegistr">
                        <?php echo Nette\Templating\Helpers::escapeHtml($template->formatAccount($ucet), ENT_NOQUOTES) ?>

                    </div>
<?php $iterations++; endforeach ;if ($opt['uctyRegistru']): $iterations = 0; foreach ($subject['ucty_registr'] as $ucet): ?>
                        <div class="ucetRegistr">
                            <?php echo Nette\Templating\Helpers::escapeHtml($template->formatAccount($ucet), ENT_NOQUOTES) ?>

                        </div>
<?php $iterations++; endforeach ;endif ?>
            </td>
<?php if ($opt['datumZverejneniUctu']): ?>
                <td style="text-align:center;">
<?php $iterations = 0; foreach ($subject['shodne_ucty'] as $ucet): ?>
                        <div class="shodnyUcet">
                            <?php echo Nette\Templating\Helpers::escapeHtml(date_format($ucet->datum_zverejneni,'d.m.Y'), ENT_NOQUOTES) ?>

                        </div>
<?php $iterations++; endforeach ;$iterations = 0; foreach ($subject['ucty_registr_shodne'] as $ucet): ?>
                        <div class="shodnyUcetRegistr">
                            <?php echo Nette\Templating\Helpers::escapeHtml(date_format($ucet->datum_zverejneni,'d.m.Y'), ENT_NOQUOTES) ?>

                        </div>
<?php $iterations++; endforeach ;if ($opt['uctyRegistru']): $iterations = 0; foreach ($subject['ucty_registr'] as $ucet): ?>
                            <div class="ucetRegistr">
                                <?php echo Nette\Templating\Helpers::escapeHtml(date_format($ucet->datum_zverejneni,'d.m.Y'), ENT_NOQUOTES) ?>

                            </div>
<?php $iterations++; endforeach ;endif ?>
                </td>
<?php endif ?>
            <td style="text-align:center;">
                <a class="jqbutton ajax registrDialogOpen" onclick="openRegistrPageWindow()" href="<?php echo htmlSpecialChars($_control->link("showRegistrPageWindow!", array($subject['dic'],$subject['dic_sap'],$subject['nazev_sap']))) ?>
">Zobrazit registr</a>
            </td>
        </tr>
<?php $iterations++; endforeach; array_pop($_l->its); $iterator = end($_l->its) ?>
        </tbody>
    </table>
</div>

<?php
}}

//
// block _regPageWindow
//
if (!function_exists($_l->blocks['_regPageWindow'][] = '_lbfad200e046__regPageWindow')) { function _lbfad200e046__regPageWindow($_l, $_args) { extract($_args); $_control->validateControl('regPageWindow')
?>        <script>
            if (<?php echo Nette\Templating\Helpers::escapeJs($regPageTitle) ?> != "") {                
                $("#registrPageWindow").dialog("option","title","Výpis z registru    ::    " + <?php echo Nette\Templating\Helpers::escapeJs($regPageTitle) ?>);
            }            
        </script>
        <div id="registrPageDiv">
          
            <?php echo $registrPage ?>

          
        </div>
<?php
}}

//
// end of blocks
//

// template extending and snippets support

$_l->extends = empty($template->_extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $template->_extended = $_extended = TRUE;


if ($_l->extends) {
	ob_start();

} elseif (!empty($_control->snippetMode)) {
	return Nette\Latte\Macros\UIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
?>
<div class="reportContainer">


<?php if ($_l->extends) { ob_end_clean(); return Nette\Latte\Macros\CoreMacros::includeTemplate($_l->extends, get_defined_vars(), $template)->render(); } ?>
<div id="<?php echo $_control->getSnippetId('reportTable') ?>"><?php call_user_func(reset($_l->blocks['_reportTable']), $_l, $template->getParameters()) ?>
</div>
</div>


<div id="registrPageWindow" title="Výpis z registru" style="z-index:1000;">
<div id="<?php echo $_control->getSnippetId('regPageWindow') ?>"><?php call_user_func(reset($_l->blocks['_regPageWindow']), $_l, $template->getParameters()) ?>
</div></div>